# eslint-config-sailsquare

## Setup

*yarn*
```bash
$ yarn add --dev eslint eslint-config-standard eslint-plugin-import eslint-plugin-node eslint-plugin-promise eslint-plugin-standard eslint-plugin-vue@beta babel-eslint
$ yarn add --dev git+ssh://git@gitlab.com/sailsquare/eslint-config.git
```
*npm*
```bash
$ npm install --save-dev eslint eslint-config-standard eslint-plugin-import eslint-plugin-node eslint-plugin-promise eslint-plugin-standard eslint-plugin-vue@beta babel-eslint
$ npm install --save-dev git+ssh://git@gitlab.com/sailsquare/eslint-config.git
```

## .eslintrc.json

```json
{
    "extends": [
        "@sailsquare/eslint-config-sailsquare"
    ]
}
```


